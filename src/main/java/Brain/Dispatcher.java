package Brain;

import Exception.*;
import Interface.View;
import Resault.Result;
import View.*;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Dispatcher {
    private final Config config;
    private final Handler handler;
    private final List<View> views;
    private final ViewResolver viewResolver;
    private final String destination;
    private final FileView fileView;
    private final ConsoleView consoleView;

    public Dispatcher(String[] array) throws ConfigException {
        this.config = new Config(array);
        this.handler = new Handler();
        this.viewResolver = new ViewResolver();
        this.destination = config.getDestination().toString();
        this.fileView = new FileView(destination);
        this.consoleView = new ConsoleView();
        this.views = new ArrayList<>(Arrays.asList(consoleView, fileView));
    }

    public void dispatch() throws DispatchException, IOException {
        Result result = new Result();
        String resaultText = config.getText4Search();
        if (config.getDestination().isFile()) {
            result.addMatches(config.getDestination(), handler.handle(config.getDestination(), config.getText4Search()));
        }
        if (config.getDestination().isDirectory()) {
            searchFiles(config.getDestination(), result);
        }

        String totalResaultText = viewResolver.resolve(result, resaultText);


        for (View view : views) {
            view.print(totalResaultText);
        }
    }

    public void searchFiles(File mainFile, Result result) {
        File[] directoryFiles = mainFile.listFiles();
        if (directoryFiles != null) {
            for (File file : directoryFiles) {
                if (file.isDirectory()) {
                    searchFiles(file, result);
                } else {
                    result.addMatches(file, handler.handle(file, config.getText4Search()));
                }
            }
        }
    }
}