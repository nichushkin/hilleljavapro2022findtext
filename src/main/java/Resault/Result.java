package Resault;

import Interface.ResaultItem;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Result {
    private final Map<File, List<ResaultItem>> matches = new HashMap<>();

    public void addMatches(File file, List<ResaultItem> list) {
        if (!list.isEmpty()) {
            matches.put(file, list);
        }
    }

    public Map<File, List<ResaultItem>> getMatches() {
        return matches;
    }

    public boolean isEmpty() {
        return matches.isEmpty();
    }
}
