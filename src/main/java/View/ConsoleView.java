package View;

import Interface.View;

public class ConsoleView implements View {

    public void print(String text) {
        System.out.println(text);
    }
}
