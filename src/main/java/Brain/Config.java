package Brain;

import java.io.*;

import Exception.*;
import Interface.LocatResault;

public class Config implements LocatResault {
    private File destination;
    private String text4Search;

    public Config(String[] array) throws ConfigException {
        try {
            parseArgs(array);
        } catch (Exception e) {
            throw new ConfigException("Не определены аргументы");
        }
    }

    public File getDestination() {
        return destination;
    }

    public String getText4Search() {
        return text4Search;
    }

    private void parseArgs(String[] array) throws ConfigException {
        this.text4Search = array[0];
        if (text4Search.isEmpty()) {
            throw new ConfigException("нет текста для поиска");
        }

        this.destination = new File(array[1]);
        if (!destination.exists()) {
            throw new ConfigException("нет файла / ов для поиска");
        }
        locationResault();
    }

    @Override
    public void locationResault() {
        if (getDestination().isDirectory()) {
            File oldFile = new File(getDestination(), "resault.txt");
            killerFileResault(oldFile);
        } else if (getDestination().isFile()) {
            File oldFile = new File(getDestination().getParent(), "resault.txt");
            killerFileResault(oldFile);
        }
    }

    public void killerFileResault(File oldFile) {
        if (oldFile.exists()) {
            oldFile.delete();
        }
    }
}