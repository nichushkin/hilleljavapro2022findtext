package Interface;


import java.io.File;
import java.util.List;

public interface Parser {
    public List<String> getSupportedExtensions();

    public boolean isSupportFile(String text);

    public List<ResaultItem> parse(File file, String text);
}
