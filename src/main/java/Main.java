import Brain.Dispatcher;
import Exception.*;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws ConfigException, IOException, DispatchException {
        Dispatcher dispatcher = new Dispatcher(args);
        dispatcher.dispatch();
    }
}
