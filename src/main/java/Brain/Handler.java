package Brain;

import Interface.Parser;
import Parsers.TxtParser;
import Parsers.XmlParser;
import Interface.ResaultItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Handler {
    private final List<Parser> parsers = new ArrayList<>();

    public Handler() {
        parsers.add(new TxtParser());
        parsers.add(new XmlParser());
    }

    public List<ResaultItem> handle(File file, String text) {
        List<ResaultItem> resault = new ArrayList<>();
        for (Parser element : parsers) {
            if (element.isSupportFile(file.getName().substring(file.getName().lastIndexOf('.') + 1))) {
                resault = element.parse(file, text);
            }
        }
        return resault;
    }
}
