package Parsers;

import Interface.ResaultItem;
import Resault.SimpleResaultItem;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TxtParser extends AbstractParser {
    private final FileReader fileReader;

    public TxtParser() {
        super(List.of("txt"));
        this.fileReader = new FileReader();
    }

    public List<ResaultItem> parse(File file, String text) {
        List<ResaultItem> list = new ArrayList<>();
        List<String> someText = fileReader.readLines(file);
        int counter = 0;
        for (String element : someText) {
            counter++;
            if (element.toLowerCase(Locale.ROOT).contains(text)) {
                SimpleResaultItem simpleResaultItem = new SimpleResaultItem(counter, element);
                list.add(simpleResaultItem);
            }
        }
        return list;
    }
}
