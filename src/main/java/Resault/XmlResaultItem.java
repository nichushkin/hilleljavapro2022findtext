package Resault;

import Interface.ResaultItem;

public class XmlResaultItem implements ResaultItem {
    private String node;

    public XmlResaultItem(String node) {
        this.node = node;
    }

    public String getFormatted() {
        StringBuilder sb = new StringBuilder();
        sb.append("Тег в котором найдено совпадение: ").append(node);
        return sb.toString();
    }
}
