package View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import Exception.DispatchException;
import Interface.LocatResault;
import Interface.View;

public class FileView implements View, LocatResault {
    private String path;

    public FileView(String path) {
        this.path = path;
    }

    @Override
    public void print(String text) throws DispatchException {
        locationResault();
        File file = new File(path, "resault.txt");
        try (OutputStream os = new FileOutputStream(file, true)) {
            os.write(text.getBytes());
        } catch (IOException e) {
            throw new DispatchException("Отказано в доступе");
        }
    }

    @Override
    public void locationResault() {
        if (new File(path).isFile()) {
            path = new File(path).getParent();
        }
    }

}
