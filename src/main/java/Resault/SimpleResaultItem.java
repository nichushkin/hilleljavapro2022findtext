package Resault;

import Interface.ResaultItem;

public class SimpleResaultItem implements ResaultItem {
    private final String line;
    private final int lineNumber;

    public SimpleResaultItem(int lineNumber, String line) {
        this.lineNumber = lineNumber;
        this.line = line;
    }

    public String getFormatted() {
        StringBuilder sb = new StringBuilder();
        sb.append("Номер строки ").append(lineNumber).append(", текст: ").append(line);
        return sb.toString();
    }
}
