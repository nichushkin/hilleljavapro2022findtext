package Parsers;

import Interface.Parser;

import java.util.Arrays;
import java.util.List;

public abstract class AbstractParser implements Parser {
    private final List<String> supportedExtensions;

    public AbstractParser(List<String> supportedExtensions) {
        this.supportedExtensions = supportedExtensions;
    }

    @Override
    public List<String> getSupportedExtensions() {
        return supportedExtensions;
    }

    @Override
    public boolean isSupportFile(String text) {
        return getSupportedExtensions().contains(text);
    }
}