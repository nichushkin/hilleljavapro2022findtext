package Brain;

import Resault.Result;

public class ViewResolver {
    public String resolve(Result result, String text) {
        StringBuilder sb = new StringBuilder();
        if (result.isEmpty()) {
            text = "Нет совпадений";
        } else {
            int counter = 0;
            sb.append("Текст для поиска: ").append(text).append("\n");
            for (var entry : result.getMatches().entrySet()) {
                sb.append(entry.getKey()).append(" -} \n");
                for (var someValue : entry.getValue()) {
                    counter++;
                    sb.append(" -> ").append(someValue.getFormatted()).append("\n");
                }
            }
            sb.append("Всего совпадений = ").append(counter).append("\n");
            text = sb.toString();
        }
        return text;
    }
}
