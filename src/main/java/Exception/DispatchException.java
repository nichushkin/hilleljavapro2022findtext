package Exception;

public class DispatchException extends Exception {

    public DispatchException(String message) {
        super(message);
    }

}
