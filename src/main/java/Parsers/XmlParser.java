package Parsers;

import Interface.ResaultItem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import Resault.XmlResaultItem;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;

public class XmlParser extends AbstractParser {

    public XmlParser() {
        super(List.of("xml"));
    }

    @Override
    public List<ResaultItem> parse(File file, String text) {
        List<ResaultItem> list = new ArrayList<>();
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            Node rootNode = document.getFirstChild();
            if (rootNode.hasChildNodes()) {
                NodeList nodeList = rootNode.getChildNodes();
                stepsXmlNode(nodeList, text, list);
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static List<ResaultItem> stepsXmlNode(NodeList nodeList, String text, List<ResaultItem> list) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            if (nodeList.item(i).getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            Node node = nodeList.item(i);
            if (node.hasChildNodes()) {
                stepsXmlNode(node.getChildNodes(), text, list);
                if (node.getTextContent().contains(text)) {
                    XmlResaultItem xmlResaultItem = new XmlResaultItem(node.getNodeName());
                    list.add(xmlResaultItem);
                }
            }
        }
        return list;
    }
}