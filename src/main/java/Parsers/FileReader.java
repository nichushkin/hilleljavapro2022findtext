package Parsers;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class FileReader {
    public FileReader() {
    }

    public List<String> readLines(File file) {

        StringBuilder sb = new StringBuilder();
        try (InputStream is = new FileInputStream(file)) {
            InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
            int bytesReader;
            while ((bytesReader = isr.read()) != -1) {
                sb.append((char) bytesReader);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String someText = sb.toString();
        return new ArrayList<>(Arrays.asList((someText).split("\\n")));
    }
}
